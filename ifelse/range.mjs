/*  a) age  0 to 18   => Underage
age  19 to 60  => Adult
age  61 to 150=> Old
else none */

let age = 123;
//console.log("age is: ", age)
console.log(`age is ${age}`)

if (age>=0 && age <=18){
    console.log("Underage")
}
else if (age>=19 && age <=60){
    console.log("Adult")
}
else if (age>=61 && age <=150){
    console.log("Old")
}
else{
    console.log("None")
}

/*	b)	if age is 25,  console your ticket is free
if age is 26,  console your ticket cost 100
if age is 27,  console your ticket cost 200
if age is other than 25,26,27 console you are not allowed
		c)      if age [from 1 to 17],  console your ticket is free
			if age[18 to 25 ],  your ticket cost 100
			else,  your ticket cost 200
*/
if (age===25){
    console.log("Your ticket is free")
}
else if (age===26){
    console.log("Your ticket costs 100")
}
else{
    console.log("None")
}


/*	c)      
