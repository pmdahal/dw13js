//for primitive
console.log(typeof "pmd")
console.log(typeof 1)
console.log(typeof true)
//console.log(typeof a)
let a=9
console.log(typeof a)

//for non primitive, type is always object
console.log(typeof [1, 2])
console.log(typeof {name: "pmd"})