let info = {
    name: "pmdahal",
    age: 33,
    isMarried: false,
}
console.log(info.name)
console.log(info.age)
console.log(info.isMarried)

let {name, age, isMarried} = {name: "pmdahal", age: 33, isMarried: false}
//in object destructuring, order does not matter
//name must match.
console.log(name)
console.log(age)
console.log(isMarried)

