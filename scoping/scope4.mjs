{//parent
    let a = 2
    let b = 4
    {//child
        let b = 6
        console.log(a)
        console.log(b)
    }
    console.log(a)
    console.log(b)
}

/* 
In parent block
a = 2
b = 4

In child block
b = 6
*/