//a variable will be known within its block only, from the line where it is defined.

{
    //console.log(a) // used before it is declared, so gives error.
    let a = 3
    console.log(a) // is defined inside the scope of {} and gives output.
}
//console.log(a) // is not defined outside the scope of {} and gives error

