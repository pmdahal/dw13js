{
let a = 3
console.log(a)
{
    //console.log(a)
    let a = 5
    console.log(a)
}
console.log(a)
}


/* 
When a variable is called:
        First, it is checked in its own block.
        If not found in the same block, then it is checked in the parent
*/

/* In other words, lexical scope refers to the ability of a function scope to access variables from the parent scope. 
When there is lexical scope, the innermost, inner and outermost functions may access all variables from their parent scopes all the way up to the global scope. */