{//parent
    let a = 9
    console.log(a)
    {//child
        a = 99
        console.log(a)
    }
    console.log(a)
}


//when a variable is called/ modified without declaring it in a child.
//it can access/ change the variable in the parent block. 