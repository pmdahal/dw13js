{
    //parent
    let a = 1
    {
        //child
        let a =11
    }
}

/* 

a variable can not be redefined in the same block
but same variable can be redefined in different blocks.

*/