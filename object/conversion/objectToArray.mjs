let info = {
    name: "pmdahal",    // key: value
    age: 33,
    isMarried: false,
}

let keysArray = Object.keys(info) // gives array of keys: [ 'name', 'age', 'isMarried' ]
console.log(keysArray)

let valuesArray = Object.values(info) // gives array of values: [ 'pmdahal', 33, false ]
console.log(valuesArray)

let propertyArray = Object.entries(info)
console.log(propertyArray)