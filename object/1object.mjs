//let info = ["pmdahal", 33, true]

let info = {
    name: "pmdahal",    // key: value
    age: 33,
    isMarried: false,
}
//get values
console.log(info)
console.log(info.name)
console.log(info.age)
console.log(info.isMarried)

//array is the collection of values.
//object is the collection of key, value pairs.
//key value pair are called property.

//change value
info.age = 22
info.isMarried = true
console.log(info)

//delete

delete info.isMarried
console.log(info)