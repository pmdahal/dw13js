let info ={
    name: "pmdahal",
    favFruits: ["apple", "grapes"],
    location: {
        country: "nepal",
        province: "bagamati",
        city: "lalitpur"
    },
    age: ()=>{
        console.log("I am 33")
    },
}

console.log(info)
console.log(info.favFruits)
console.log(info.favFruits[1])
console.log(info.location)
console.log(info.location.province)
info.age()
