//let date = new Date()
//console.log(date)

console.log(new Date())
//it gives current date and time.

//new Date() gives date in ISO format
//yyyy-mm-ddThh:mm:ss.milisecondsZ : where Z is the UTC time zone
//2024-3-5T5-6-7 : invalid
//2024-03-05T17:48:33 : valid

console.log(new Date().toLocaleString()) //Local Date and Time
console.log(new Date().toLocaleDateString()) //Local Date
console.log(new Date().toLocaleTimeString()) //Local Time