/* 
callstack
    callstack runs code inside it
    it works in fifo principle
    setTimeout() code goes to memory, and after the defined delay goes to memory queue
event loop
    it is a mediator which continuously monitor callstack and memory queue.
    if callstack is empty, it pushes the function from memory queue to callstack.

Why memory queue?
    
*/