let product = [
    {
        name: "mobile",
        price: 1000
    },
    {
        name: "laptop",
        price: 1500
    },
    {
        name: "tablet",
        price:500
    }
]

let arrayName = product.map((value, index)=>{
    return value.name
}) //[ 'mobile', 'laptop', 'tablet' ]
console.log(arrayName)

let arrayPrice = product.map((value, index)=>{
    return value.price
}) //[[ 1000, 2000, 3000 ]
console.log(arrayPrice)


