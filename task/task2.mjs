// make a arrow function named canEnter, pass a value , the function must return true if the given age either 18 or 25 or 30 else return false

let canEnter = (num)=>{
    if (num === 18 || num === 25 || num === 30)
    return true
    else
    return false
    }
    
    console.log(canEnter(33))

// make a arrow function that takes one input as number and return "category1" for number range from 1 to10,  return "category2" for number range from 11 to 20, return "category3" for number range form 21 to 30

let numCategory = (num)=>{
    if (num >= 1 && num <= 10){
    return "category 1"
    }
    else if (num >= 11 && num <= 20){
    return "category 2"
    }
    else if (num >= 21 && num <= 30){
        return "category 3"
        }
}
let _numCategory = numCategory(22)
console.log(_numCategory)