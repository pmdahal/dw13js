import { capitalizeFirst } from "./task8HwCaseConv.mjs"

let capitalizeFirstLetter = (name)=>{
    let str2Arr = name.split(" ")
    //return (str2Arr)

    let _str2Arr = str2Arr.map((value, index)=>{
        return capitalizeFirst(value)
    })
    let arr2Str = _str2Arr.join(" ")
    return arr2Str
           
}
console.log(capitalizeFirstLetter("hello there here i am this is me"))
