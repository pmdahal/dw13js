const products = [
    {
        id: 1,
        title: "MacBook Pro",
        category: "Laptops",
        price: 100000.00,
        description: "A high-performance laptop.",
        manufactureDate: "2023-05-15T08:30:00",
        isAvailable: true
    },
    {
        id: 2,
        title: "Nike",
        category: "Running Shoes",
        price: 5000,
        description: "Running shoes designed for speed and comfort.",
        manufactureDate: "2023-02-20T14:45:00",
        isAvailable: true
    },
    {
        id: 3,
        title: "Python",
        category: "Books",
        price: 500,
        description: "A language for AI",
        manufactureDate: "1925-04-10T10:10:00",
        isAvailable: false
    },
    {
        id: 4,
        title: "Javascript",
        category: "Books",
        price: 700,
        description: "A language for Browser",
        manufactureDate: "1995-12-04T12:00:00",
        isAvailable: false
    },
    {
        id: 5,
        title: "Dell XPS",
        category: "Laptops",
        price: 120000.00,
        description: "An ultra-slim laptop with powerful performance.",
        manufactureDate: "2023-04-25T09:15:00",
        isAvailable: true
    }
];

/* let _title = products.map((value, index)=>{
    return value.title
})
console.log(_title)

let _category = products.map((value, index)=>{
    return value.category
})
console.log(_category)

let description = products.map((value, index)=>{
    return `${value.title} costs ${value.price} and is of the category ${value.category}`
})
console.log(description) */

//show products with price greater than 2000.

let filteredProducts = products.filter((value, index)=>{
    if (value.price>2000)
    return value
})
let detail = filteredProducts.map((value, index)=>{
        return `${value.title} costs ${value.price} and is of the category ${value.category}`
})
console.log(detail)

let _filteredProducts = products.filter((value, index)=>{
    if (value.category === "Books")
    return value
})
let _detail = _filteredProducts.map((value, index)=>{
        return `${value.title} costs ${value.price} and is of the category ${value.category}`
})
console.log(_detail)

//no need to declare new variable _detail, .map can be called directly at the end of the filter process

/* let priceArray = products.map((value, index)=>{
    return value.price
})
console.log(priceArray)

let totalPrice = priceArray.reduce((pre, cur)=>{
    return cur + pre
}, 0)
console.log(totalPrice)
*/

let totalPrice = products.reduce((pre, cur)=>{
    return pre + cur.price
}, 0)
console.log(totalPrice)
