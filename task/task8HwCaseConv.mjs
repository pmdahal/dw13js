export let capitalizeFirst = (name)=>{
    //return name.toUpperCase()
    let nameArray = name.split("")
    //method one
    /* nameArray[0] = nameArray[0].toUpperCase()
    let nameString = nameArray.join("")
    return nameString */
    //create new array by capitalizing first letter
    let outputArray = nameArray.map((value, index)=>{
        if (index===0) 
        return value.toUpperCase();
        else
        return value.toLowerCase()
    })
    let nameString = outputArray.join("")
    return nameString
}
//console.log(capitalizeFirst("pmdahal"))
