//make a arrow function that take a number and return you can enter room only if the enter number is less than 18 else you can not enter
let canEnter = (value)=>{
    if (value <18){
        return "you can enter the room"
    }
    else
        return "you can not enter"
} 

let _canEnter = canEnter(18)
console.log(_canEnter)


//make a arrow function that takes a sentence and return total number of character in that sencence (using string.length)

let countChar = (sentence)=>{
    return sentence.length
} 

let _countChar = countChar("pmdahal")
console.log(_countChar)