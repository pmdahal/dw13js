//DATA => primitive, non-primitive
//primitive: number, string, boolean, undefined, null
//non-primitive: object, array, date, error

//primitive: number, string, boolean, undefined, null
//memory allocation
//if let is used, memory is allocated for that
//in case of primitive, === sees values.

let a = 1
let b = a
a = 2
let c = 1
console.log(a)
console.log(b)
console.log(c)
console.log(a===b)
console.log(b===c)

//non-primitive: object, array, date, error
//memory allocation
//before allocating memory, it checks whether the variable is copy of another variable
// if the variable is a copy of another, it will share memory
//in case of non-primitive, === sees memory address

let ar1 = [1, 2, 3]
let ar2 = ar1
let ar3 = [1, 2, 3]
ar1.push(4)
console.log(ar1)
console.log(ar2)
console.log(ar3)
console.log(ar1===ar2)
console.log(ar1===ar3)
