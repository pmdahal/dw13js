//difference between null and undefined
//undefined means variable is defined but not initialized.
//null means variable is defined and initialized with null.

//undefined is a primitive value that represents the absence of
//any value, while null is another primitive value that represents 
//the intentional absence of any value.

let num
console.log(num)
num = 5
console.log(num)
num = undefined
console.log(num)

num = null //
console.log(num)