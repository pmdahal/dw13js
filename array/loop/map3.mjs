//[1, 2, 3] => [3, 6, 9]
let input = ["my", "name", "is"]

let output = input.map((value, index)=>{
    return value.toUpperCase() 
})
console.log(output)


let _output = input.map((value, index)=>{
    return `${value.toUpperCase()}N`
})
console.log(_output)