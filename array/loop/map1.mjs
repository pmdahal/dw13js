//[1, 2, 3] => [3, 6, 9]
let input = [1, 2, 3]

let output = input.map((value, index)=>{
    return value*3 
})
console.log(output)


//[1, 2, 3] => [11, 12, 13]

let __output = input.map((value, index)=>{
    return value+10 
})
console.log(__output)

//[1, 2, 3] => ["1111", "2111", "3111"]
let _output = input.map((value, index)=>{
    return `${value}111`
})
console.log(_output)