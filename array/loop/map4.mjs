//[1, 2, 3] => ["1000a", "2111a", "3222a"]
let input = [1, 2, 3]

let output = input.map((value, index)=>{
    return `${value}${index}${index}${index}a`
})
console.log(output)

//[1, 3, 4, 5] => [100, 300, 0, 500]

let _input = [1, 3, 4, 5]
let _output = _input.map((value, index)=>{
    if (value%2 === 0){
        return value*0
    }
    else{
        return value*100
    }
})
console.log(_output)