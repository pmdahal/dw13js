let nums = [10, 20, 30]

let nums1 = nums.map((value, index)=>{
    return value*2 // [20, 40, 60]
}) //returns a value for each steps of the loop
// output of the map function has same length as input

console.log(nums1)