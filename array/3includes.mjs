let ar1 = ["pmd", 33, false]

let v=ar1.includes("ram")
console.log(v) //false

let v1=ar1.includes("pm")
console.log(v1) //false

let v2=ar1.includes(33)
console.log(v2) //false


/* 

nitanthapa425
  5:38 PM
//make a arrow function
// pass array of fruits
// the function must return "the fruits contain apple." if the array contain "apple"
//  else return "the fruits do not contain apple."
*/

let hasApple = (fruits)=>{
    let _hasApple = fruits.includes("apple")
    if (_hasApple)
        console.log("the fruits contain apple")
    else
        console.log("the fruits do not contain apple") 
}

let __hasApple = hasApple(["banana", "orange", "apple"])

// make a arrow function
// pass array of names
// the fun must return "arrays length is greater than 3"  if passes array length is greater than 3
//else the fun must return "array length is less than 3"

let arrLen = (names)=>{
    if (names.length>3)
    return "arrays length is greater than 3"
    else if (names.length<3)
    return "arrays length is less than 3"
    else 
    return "arrays length is equal to 3"
}

//let _arrLen = arrLen(["abc", "def", 3, false])
//console.log(_arrLen)
console.log(arrLen(["abc", "def", 3, true]))
