let ar1 = ["pmd", 33, true]
console.log(ar1)
console.log(ar1[0])

ar1.shift()
console.log(ar1)
console.log(ar1[0])
//shift removes element at the beginning of the array.
//our sequence is push, pop, unshift, shift