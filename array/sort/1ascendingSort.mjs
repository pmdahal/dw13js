let arr1 = ["b", "d", "a", "c"]
let arr2 = arr1.sort()
console.log(arr2)

let arr3 = ["ab", "aa", "a", "ad"]
let arr4 = arr3.sort()
console.log(arr4)

let arr5 = [2, 10]
let arr6 = arr5.sort()
console.log(arr5)
//sort function works only for strings, and not for numbers.
//1, 8 => 1, 8
//2, 10 => 10, 2

let arr7 = ["a", "A", "B", "z"]
let arr8 = arr7.sort()
console.log(arr8) //[ 'A', 'B', 'a', 'z' ]
//capital letters come before small letters in sort function.