//join converts array to string
let ar1 = ["p", "m", "d", "a", "h", "a", "l"]

//let v= ar1.join("*")
//console.log(v)
console.log(ar1.join("*"))
console.log(ar1.join(" "))
console.log(ar1.join("ram"))
console.log(ar1.join())
console.log(ar1.join("")) //

//input => array
//output => string
