let ar1 = ["abc", "d", "pmd"]
let ar2 = [1, 5, 9]

let _ar = [...ar1, "xyz"]
let __ar = [3, 7, ...ar2, 4, 8, ...ar1]
let ___ar = [...ar1, ...ar2]
let ____ar = [...ar2, ...ar1]

console.log(_ar)
console.log(__ar)
console.log(___ar)
console.log(____ar)

//spread operator are the wrapper opener.

