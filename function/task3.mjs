/* function that has two parameters
adds them
returns the sum
*/

let sum = (num1, num2)=>{
    //let num3 = num1+num2
    //return(num3)
    return num1+num2  // it is better approach
}

let _sum = sum(2, 3)
console.log(`SUM: ${_sum}`)

let product = (num1, num2)=>{
    return num1*num2
}

let _product = product(2,3)
console.log(`PRODUCT: ${_product}`)
console.log(`PRODUCT: ${product(2,3)}`)

let average = (num1, num2, num3)=>{
    return (num1+num2+num3)/3
}

let _average = average(1, 2,3)
console.log(`AVERAGE: ${_average}`)

let difference = (num1, num2)=>{
    return num1-num2
}

let _difference = difference(1, 3)
console.log(`DIFFERENCE: ${_difference}`)

let division = (num1, num2)=>{
    return num1/num2
}

let _division = division(1, 3)
console.log(`DIVISION: ${_division}`)