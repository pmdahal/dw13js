let a;
console.log(a) // for variable with unassigned value, undefined will be stored in it.

let fun1 = ()=>{
    console.log("A")
    console.log("B")
}

let s = fun1()
console.log(s)