let a=1 // data is a number
let b="pmd" // data is a string
let c = ()=>{   // data is a function
    return "I am a function."
}
console.log(a)
console.log(b)

let data = c()
console.log(data) //assigning and printing

console.log(c()) // direct printing
