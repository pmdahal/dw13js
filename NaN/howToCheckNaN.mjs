let a = NaN
console.log(a === NaN)
//we can not check in this way

//to check whether a variable is NaN or not, we have to use isNaN()
console.log(isNaN(a))
